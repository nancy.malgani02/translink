﻿Feature: ApiDemo
	In order to verify search api
	As a tester
	I want to be able to make get request using rest assured

@RestApiDemo
Scenario: Verify location api for given city
	Given the user access google location api to search city 'Vancouver'
	Then the user should receive http code '200'
	And the user should see the response contains searched city 'Vancouver'