﻿Feature: WebsiteDemo
	In order to ensure clients can contact us
	As a tester
	I want to verify presence of mandatory fields on contact us page

@WebsiteTesting
Scenario: Verify Contact us page
	Given I launch the website
	When I navigate to contact us
	And I scroll to BC Ferries Vacations on bottom of page
	Then I should see Address as 'Surrey, BC V3T 5T3'
	And I should see Phone as 'T: 778.375.6400'