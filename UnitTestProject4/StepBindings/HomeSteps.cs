﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RA;
using TechTalk.SpecFlow;

//Author : Nancy Malgani

namespace UnitTestProject4
{
    [Binding]
    [TestClass]
    public class HomeSteps
    {
        private HomePage homePage = new HomePage();

        [Given(@"I launch the website")]
        public void GivenILaunchTheWebsite()
        {
            homePage.LaunchWebsite();
        }

        [When(@"I navigate to contact us")]
        public void WhenINavigateToContactUs()
        {
            homePage.NavigateToContactUs();
        }

        [When(@"I scroll to BC Ferries Vacations on bottom of page")]
        public void WhenIScrollToBCFerriesVacationsOnBottomOfPage()
        {
            homePage.ScrollToBottomOfPage();
        }


        [Then(@"I should see Hours as '(.*)'")]
        public void ThenIShouldSeeHoursAs(string p0)
        {
            homePage.VerifyTextHours(p0);
        }

        [Then(@"I should see Address as '(.*)'")]
        public void ThenIShouldSeeAddressAs(string p0)
        {
            homePage.VerifyTextAddress(p0);
        }


        [Then(@"I should see Phone as '(.*)'")]
        public void ThenIShouldSeePhoneAs(string p0)
        {
            homePage.VerifyTextPhone(p0);
            homePage.TearDownAfterScenario();
        }

        [AfterScenario]
        public void TearDown()
        {
            //homePage.TearDownAfterScenario();
        }

    }
}

