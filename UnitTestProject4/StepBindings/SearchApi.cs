﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RA;
using TechTalk.SpecFlow;

//Author : Nancy Malgani

namespace UnitTestProject4
{
    [Binding]
    [TestClass]
    public class SearchApi
    {
        
        [Given(@"the user access google location api to search city '(.*)'")]
        public void GivenTheUserAccessGoogleLocationApiToSearchCity(string p0)
        {
            //ScenarioContext.Current.Pending();
        }


        [Then(@"the user should receive http code '(.*)'")]
        public void ThenTheUserShouldReceiveHttpCode(int p0)
        {
            //ScenarioContext.Current.Pending();
        }

        [Then(@"the user should see the response contains searched city '(.*)'")]
        public void ThenTheUserShouldSeeTheResponseContainsSearchedCity(string p0)
        {
                new RestAssured()
                .Given()
                .Name("Search Api Test Suite")
                .Header("Content-Type", "application/json")
                .Header("Authorization", "Basic MzA0NzY4MjY4OTpJUEhPTkU6")
                .Uri("")
                .Debug()
                .When()
                .Get(("https://maps.googleapis.com/maps/api/geocode/json?address=" + p0 + "&sensor=false&238221" + "&key=AIzaSyB0z1TVEHzk5ZRG8luQbAkvfmvy1j2gWfg"))
                .Debug()
                .Then()
                .TestBody("Validate response", responseStatus => responseStatus.status == "OK")
                .Debug()
                .Assert("Validate response")
                ;
        }


        
    }
}

